import React from "react";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";
import {
  Flex,
  Text,
  Input,
  FormControl,
  FormLabel,
  Button,
  FormHelperText,
} from "@chakra-ui/react";
import Constants from "../Constants/Constants";
import Context from "../Context/Context";
import Router from "next/router";
class Login extends React.Component {
  constructor() {
    super();
    this.state = { isButtonDisabled: true, errorMessage: "", isError: false };
  }
  handleMobileNumber = (event) => {
    if (Number(event.target.value) && event.target.value.length === 10) {
      this.setState({
        phoneNumber: Number(event.target.value),
        isError: false,
        errorMessage: "",
        isButtonDisabled: false,
      });
    } else {
      this.setState({
        isButtonDisabled: true,
        isError: true,
        errorMessage: "Please Enter A valid Number",
      });
    }
  };
  handleAPI = (event) => {
    event.preventDefault();
    this.setState({ isButtonDisabled: true });
    //API Fetch
    fetch(`${Constants.BASEURL}/sendOtp`, {
      method: "post",
      body: {
        mobile: this.state.phoneNumber,
        countryCode: 91,
      },
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        this.setState({ isButtonDisabled: false });
        this.context.updateContext([
          { key: "id", value: data.data.id },
          { key: "otp", value: data.data.otp },
        ]);
        Router.push("/verify");
      })
      .catch((e) => {
        console.log(e);
      });
  };
  render() {
    return (
      <>
        <div id="tg-wrapper" class="tg-wrapper tg-haslayout tg-openmenu">
          <Header hideLoginButton={true} />
          <main id="tg-main" class="tg-main tg-haslayout">
            <div style={{ minHeight: "450px" }} class="container">
              <Flex
                borderRadius="10px"
                height="250px"
                m="auto"
                mt="90px"
                width="500px"
                boxShadow="0 0 5px #696969"
              >
                <Flex
                  pt="40px"
                  margin="auto"
                  width="400px"
                  display="block"
                  minHeight="500px"
                >
                  <Text
                    mb="20px"
                    fontFamily="inherit"
                    fontSize="5xl"
                    fontWeight="bold"
                    color="rgb(102,102,102)"
                  >
                    Login
                  </Text>
                  <Flex alignItems="center">
                    <FormControl>
                      <FormLabel>Mobile Number</FormLabel>
                      <Input
                        onChange={this.handleMobileNumber}
                        borderRadius="10px"
                        type="text"
                      />
                      <FormHelperText>We'll never share.</FormHelperText>
                      {this.state.isError ? (
                        <Text fontSize="12px" color="red">
                          {this.state.errorMessage}
                        </Text>
                      ) : null}
                    </FormControl>
                  </Flex>
                  <Button
                    h="43px"
                    w="95px"
                    fontSize="14px"
                    mt="10px"
                    borderRadius="20px"
                    color="white"
                    disabled={this.state.isButtonDisabled}
                    background="#00CC67"
                    _hover={{
                      bg: "#0BA352",
                    }}
                    onClick={this.handleAPI}
                  >
                    Login
                  </Button>
                </Flex>
              </Flex>
            </div>
          </main>
          <Footer />
        </div>
      </>
    );
  }
}
Login.contextType = Context;
export default Login;
