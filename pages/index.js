import React from "react";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";
import Slider from "../Components/Slider/Slider";
import Categories from "../Components/Categories/Categories";
import Product from "../Components/Product/Product";
import Constants from "../Constants/Constants";
export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      products: [],
    };
  }
  static async getInitialProps(ctx) {
    const res = await fetch(`${Constants.BASEURL}/products`);
    const json = await res.json();
    return { products: json.data.data };
  }
  componentDidMount = () => {
    this.setState({ products: this.props.products });
  };
  render() {
    return (
      <>
        <div id="tg-wrapper" class="tg-wrapper tg-haslayout tg-openmenu">
          <Header />
          <Slider />
          <main id="tg-main" class="tg-main tg-haslayout">
            <Categories />
            <section class="tg-sectionspace tg-haslayout">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                      <div class="tg-title">
                        <h2>Tag Line</h2>
                      </div>
                      <div class="tg-description">
                        <p>Over 10,56,432 Featured Ads</p>
                      </div>
                    </div>
                  </div>
                  <div class="tg-ads">
                    {this.state.products.map((_product) => (
                      <Product product={_product} />
                    ))}
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-btnbox">
                      <a class="tg-btn" href="javascript:void(0);">
                        View All
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </main>
          <Footer />
        </div>
      </>
    );
  }
}
