import React from "react";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";
import {
  Flex,
  Text,
  Input,
  FormControl,
  FormLabel,
  Button,
  FormHelperText,
  background,
} from "@chakra-ui/react";
import Context from "../Context/Context";
import Constants from "../Constants/Constants";
class Verify extends React.Component {
  constructor() {
    super();
    this.state = {
      isButtonDisabled: true,
      otp: undefined,
      isError: false,
      errorMessage: "",
    };
  }
  componentDidMount = () => {
    console.log(`your OTP is ${this.context.otp}`);
  };
  handleOtp = (event) => {
    if (Number(event.target.value)) {
      this.setState({
        otp: Number(event.target.value),
        isButtonDisabled: false,
        isError: false,
      });
    } else {
      this.setState({
        isError: true,
        errorMessage: "Enter a valid OTP",
        isButtonDisabled: true,
      });
    }
  };
  verifyOtp = () => {
    fetch(`${Constants.BASEURL}/verifyOtp`, {
      method: "post",
      body: {
        id: this.context.id,
        otp: this.state.otp,
      },
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        this.setState({ isButtonDisabled: false });
        console.log(data);
        // Router.push("/");
      })
      .catch((e) => {
        console.log(e);
      });
  };
  render() {
    return (
      <>
        <div id="tg-wrapper" class="tg-wrapper tg-haslayout tg-openmenu">
          <Header hideLoginButton={true} />
          <main id="tg-main" class="tg-main tg-haslayout">
            <div style={{ minHeight: "450px" }} class="container">
              <Flex
                borderRadius="10px"
                height="250px"
                m="auto"
                mt="90px"
                width="500px"
                boxShadow="0 0 5px #696969"
              >
                <Flex
                  pt="40px"
                  margin="auto"
                  width="400px"
                  display="block"
                  minHeight="500px"
                >
                  <Text
                    mb="20px"
                    fontFamily="inherit"
                    fontSize="5xl"
                    fontWeight="bold"
                    color="rgb(102,102,102)"
                  >
                    One-Time Password
                  </Text>
                  <Flex alignItems="center">
                    <FormControl id="email">
                      <FormLabel>OTP</FormLabel>
                      <Input
                        onChange={this.handleOtp}
                        borderRadius="10px"
                        type="text"
                      />
                      <FormHelperText>Please enter otp</FormHelperText>
                      {this.state.isError ? (
                        <Text fontSize="12px" color="red">
                          {this.state.errorMessage}
                        </Text>
                      ) : null}
                    </FormControl>
                  </Flex>
                  <Button
                    h="43px"
                    w="95px"
                    fontSize="14px"
                    mt="18px"
                    borderRadius="20px"
                    color="white"
                    background="#00CC67"
                    onClick={this.verifyOtp}
                    disabled={this.state.isButtonDisabled}
                    _hover={{
                      bg: "#0BA352",
                    }}
                  >
                    Submit
                  </Button>
                </Flex>
              </Flex>
            </div>
          </main>
          <Footer />
        </div>
      </>
    );
  }
}
Verify.contextType = Context;
export default Verify;
