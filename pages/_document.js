import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <title>Karo Barter</title>
          {/* <link rel="apple-touch-icon" href="/apple-touch-icon.png"/> */}
          <link rel="stylesheet" href="/css/bootstrap.min.css" />
          <link rel="stylesheet" href="/css/normalize.css" />
          <link rel="stylesheet" href="/css/font-awesome.min.css" />
          <link rel="stylesheet" href=" /css/icomoon.css" />
          <link rel="stylesheet" href="/css/transitions.css" />
          <link rel="stylesheet" href="/css/flags.css" />
          <link rel="stylesheet" href=" /css/owl.carousel.css" />
          <link rel="stylesheet" href="/css/prettyPhoto.css" />
          <link rel="stylesheet" href="/css/jquery-ui.css" />
          <link rel="stylesheet" href="/css/scrollbar.css" />
          <link rel="stylesheet" href="/css/chartist.css" />
          <link rel="stylesheet" href="/css/main.css" />
          <link rel="stylesheet" href="/css/color.css" />
          <link rel="stylesheet" href="/css/responsive.css" />
          <script src="/assets/js/vendor/jquery-library.js"></script>
          <script src="/assets/js/vendor/bootstrap.min.js"></script>
          <script src="http://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
          <script src="/assets/js/tinymce/tinymce.min.js?apiKey=4cuu2crphif3fuls3yb1pe4qrun9pkq99vltezv2lv6sogci"></script>
          <script src="/assets/js/responsivethumbnailgallery.js"></script>
          <script src="/assets/js/jquery.flagstrap.min.js"></script>
          <script src="/assets/js/backgroundstretch.js"></script>
          <script src="/assets/js/owl.carousel.min.js"></script>
          <script src="/assets/js/jquery.vide.min.js"></script>
          <script src="/assets/js/jquery.collapse.js"></script>
          <script src="/assets/js/scrollbar.min.js"></script>
          <script src="/assets/js/chartist.min.js"></script>
          <script src="/assets/js/prettyPhoto.js"></script>
          <script src="/assets/js/jquery-ui.js"></script>
          <script src="/assets/js/countTo.js"></script>
          <script src="/assets/js/appear.js"></script>
          <script src="/assets/js/gmap3.js"></script>
          <script src="/assets/js/main.js"></script>
        </Head>
        <body class="tg-home tg-homeone">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
export default MyDocument;
