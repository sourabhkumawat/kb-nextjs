import "../styles/globals.css";
import { ChakraProvider } from "@chakra-ui/react";
import Context from "../Context/Context";
import React from "react";
class MyApp extends React.Component {
  constructor() {
    super();
    this.state = {
      Context: {
        id: undefined,
        otp: undefined,
        updateContext: this.updateContext,
      },
    };
  }
  updateContext = (stateToUpdate) => {
    const _state = this.state.Context;
    if (Array.isArray(stateToUpdate)) {
      stateToUpdate.map((_stateToUpdate) => {
        _state[_stateToUpdate.key] = _stateToUpdate.value;
      });
    } else {
      _state[stateToUpdate.key] = stateToUpdate.value;
    }
    this.setState({ Context: _state });
  };
  render() {
    return (
      <ChakraProvider>
        <Context.Provider value={this.state.Context}>
          <this.props.Component {...this.props.pageProps} />
        </Context.Provider>
      </ChakraProvider>
    );
  }
}

export default MyApp;
