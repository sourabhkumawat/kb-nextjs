import React from "react";

class Slider extends React.Component {
  render() {
    return (
      <div id="tg-homebanner" class="tg-homebanner tg-haslayout">
        <figure
          class="bg-image"
          style={{ backgroundImage: `url(/images/banner.jpeg) ` }}
          data-vide-bg={`url(/images/banner.jpeg) `}
          data-vide-options="position: 50% 50%"
        >
          <figcaption>
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="tg-bannercontent">
                    <h1>Karo Barter</h1>
                    <h2>Spread Smile Spread Happiness</h2>
                  </div>
                </div>
              </div>
            </div>
          </figcaption>
        </figure>
      </div>
    );
  }
}
export default Slider;
