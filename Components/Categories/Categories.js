import React from "react";

class Categories extends React.Component {
  render() {
    return (
      <section class="tg-haslayout">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">
              <div class="tg-categoriessearch">
                <div class="row">
                  <div class="col-sm-3 col-xs-12">
                    <a
                      href=""
                      class="col-xs-12 btn btn-primary large-action-button"
                    >
                      Buy
                    </a>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <a
                      href=""
                      class="col-xs-12 btn btn-warning large-action-button"
                    >
                      Rent
                    </a>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <a
                      href=""
                      class="col-xs-12 btn btn-success large-action-button"
                    >
                      Barter
                    </a>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <a
                      href=""
                      class="col-xs-12 btn btn-danger large-action-button"
                    >
                      Donate
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default Categories;
