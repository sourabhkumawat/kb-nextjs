import React from "react";
import Search from "../Search/Search";
import Link from "next/link";
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      auth: false,
    };
  }
  componentDidMount = () => {
    if (localStorage.getItem("token")) {
      this.setState({ auth: true });
    }
  };
  render() {
    return (
      <header id="tg-header" class="tg-header tg-haslayout">
        <div class="tg-navigationarea">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-sm-1 col-xs-12">
                  <strong class="tg-logo">
                    <Link href="/">
                      <a>
                        <img src="/images/logo.png" alt="company logo here" />
                      </a>
                    </Link>
                  </strong>
                </div>
                <Search />
                <div class="col-sm-3">
                  <div class="row">
                    <div class="col-sm-6">
                      <a
                        class="tg-btn post-ad-button"
                        href="dashboard-postanad.html"
                      >
                        <i class="icon-bookmark"></i>
                        <span>post an ad</span>
                      </a>
                    </div>
                    <div class="col-sm-6">
                      {this.state.auth ? (
                        <div class="dropdown tg-themedropdown tg-userdropdown">
                          <a
                            href="javascript:void(0);"
                            id="tg-adminnav"
                            class="tg-btndropdown"
                            data-toggle="dropdown"
                          >
                            <span class="tg-userdp">
                              <img
                                src="/images/author/img-01.jpg"
                                alt="image description"
                              />
                            </span>
                            <span class="tg-name">Welcome</span>
                          </a>
                          <ul
                            class="dropdown-menu tg-themedropdownmenu"
                            aria-labelledby="tg-adminnav"
                          >
                            <li>
                              <a href="dashboard.html">
                                <i class="icon-chart-bars"></i>
                                <span>Insights</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-profile-setting.html">
                                <i class="icon-cog"></i>
                                <span>Profile Settings</span>
                              </a>
                            </li>
                            <li class="menu-item-has-children">
                              <a href="javascript:void(0);">
                                <i class="icon-layers"></i>
                                <span>My Ads</span>
                              </a>
                              <ul>
                                <li>
                                  <a href="dashboard-myads.html">All Ads</a>
                                </li>
                                <li>
                                  <a href="dashboard-myads.html">
                                    Featured Ads
                                  </a>
                                </li>
                                <li>
                                  <a href="dashboard-myads.html">Active Ads</a>
                                </li>
                                <li>
                                  <a href="dashboard-myads.html">
                                    Inactive Ads
                                  </a>
                                </li>
                                <li>
                                  <a href="dashboard-myads.html">Sold Ads</a>
                                </li>
                                <li>
                                  <a href="dashboard-myads.html">Expired Ads</a>
                                </li>
                                <li>
                                  <a href="dashboard-myads.html">Deleted Ads</a>
                                </li>
                              </ul>
                            </li>
                            <li>
                              <a href="dashboard-postanad.html">
                                <i class="icon-layers"></i>
                                <span>Dashboard Post Ad</span>
                              </a>
                            </li>
                            <li class="menu-item-has-children">
                              <a href="javascript:void(0);">
                                <i class="icon-envelope"></i>
                                <span>Offers/Messages</span>
                              </a>
                              <ul>
                                <li>
                                  <a href="dashboard-offermessages.html">
                                    Offer Received
                                  </a>
                                </li>
                                <li>
                                  <a href="dashboard-offermessages.html">
                                    Offer Sent
                                  </a>
                                </li>
                                <li>
                                  <a href="dashboard-offermessages.html">
                                    Trash
                                  </a>
                                </li>
                              </ul>
                            </li>
                            <li>
                              <a href="dashboard-payments.html">
                                <i class="icon-cart"></i>
                                <span>Payments</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-myfavourites.html">
                                <i class="icon-heart"></i>
                                <span>My Favourite</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-privacy-setting.html">
                                <i class="icon-star"></i>
                                <span>Privacy Settings</span>
                              </a>
                            </li>
                            <li>
                              <a href="javascript:void(0);">
                                <i class="icon-exit"></i>
                                <span>Logout</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      ) : this.props.hideLoginButton ? (
                        ""
                      ) : (
                        <div class="col-sm-6">
                          <Link href="/login">
                            <a
                              class="tg-btn post-ad-button"
                              href="dashboard-postanad.html"
                            >
                              <span>Login </span>
                            </a>
                          </Link>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
export default Header;
